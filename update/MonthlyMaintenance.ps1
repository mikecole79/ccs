# Popup message box in PowerShell
# https://4sysops.com/archives/how-to-display-a-pop-up-message-box-with-powershell/
# https://michlstechblog.info/blog/powershell-show-a-messagebox/
# Add checkbox for things to do?
#
# Add chkdks, disk cleanup, and defrag to the script.

# Messagebox
# Load assembly
#[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
#$oReturn=[System.Windows.Forms.MessageBox]::Show("Message Text","Title",[System.Windows.Forms.MessageBoxButtons]::OKCancel)
#switch ($oReturn){
#    "OK" {
#        write-host "You pressed OK"
#        # Enter some code
#    }
#    "Cancel" {
#        write-host "You pressed Cancel"
#        # Enter some code
#    }
#}


Write-Host "Starting script"
Remove-Item -Recurse C:\CCS\Downloads
mkdir C:\CCS\Downloads
iwr https://www.colecomputerservices.com/update/MonthlyMaintenance.ps1 -OutFile C:\CCS\Downloads\MonthlyMaintenance.ps1

# Setup Variables
$ccleanerSettings = "[Options]
WipeFreeSpaceDrives=C:\
LatestICS=5.47.6716
CookiesToSave=*.avast.com|*.ccleaner.com|*.ccleanercloud.com|*.piriform.com|accounts.google.com|aol.com|facebook.com|google.com|login.live.com|mail.google.com|mail.yahoo.com|twitter.com|www.google.com|yahoo.com
RunICS=0
Monitoring=0
GD=950e581a-ac4b-4c0c-97a3-bba5e4e50c5c
CheckTrialOffer=0
(App)Microsoft Edge - Internet History=False
(App)Microsoft Edge - Download History=False
(App)Microsoft Edge - Last Download Location=False
(App)History=False
(App)Recently Typed URLs=False
(App)Last Download Location=False
(App)Recent Documents=True
(App)Other Explorer MRUs=True
(App)Windows Error Reporting=True
(App)DNS Cache=True
(App)Windows Event Logs=True
(App)Old Prefetch data=True
(App)Old Windows Installation=True
(App)Mozilla - Internet History=False
(App)Mozilla - Download History=False
(App)Mozilla - Last Download Location=False
(App)Google Chrome - Internet History=False
(App)Google Chrome - Download History=False
(App)Google Chrome - Last Download Location=False
UpdateKey=03/22/2020 04:30:53 PM
BackupDir=C:
ShowCleanWarning=False
WINDOW_LEFT=342
WINDOW_TOP=20
WINDOW_WIDTH=1024
WINDOW_HEIGHT=708
WINDOW_MAX=0
(App)Office 2007=True
(App)Office 2010=True
NewVersion=5.64.7613
AnalyzerTypes=1|1|1|1|1|1|1
SystemAnalyzerDrives=C:\
FinderInclude1=PATH|C:\|*.*|RECURSE|0|0|24
FinderInclude2=PATH|D:\|*.*|RECURSE|0|0|24
FinderIncludeStates=1|0|0|0|0|0|0|0|0
FinderInclude3=PATH|E:\|*.*|RECURSE|0|0|24
ShowIECleanWarning=False
ShowGoogleChromeCleanWarning=False
ShowEdgeCleanWarning=False
ShowWindowsExplorerCleanWarning=False
FinderInclude4=PATH|F:\|*.*|RECURSE|0|0|24
FinderInclude5=PATH|G:\|*.*|RECURSE|0|0|24
FinderInclude6=PATH|H:\|*.*|RECURSE|0|0|24
FinderInclude7=PATH|I:\|*.*|RECURSE|0|0|24
FinderInclude8=PATH|J:\|*.*|RECURSE|0|0|24
FinderInclude9=PATH|Q:\|*.*|RECURSE|0|0|24
SystemMonitoringRunningNotification=1
FTU=09/10/2018|16|1
PPC=D1708795FA271EA7B30176ED0576B6A9F807250DFF5E80C1B882FEB96DF67AA8
STS=MP3ZI2MWQPQS4CUDPTUYC5UIPF3ZI55URE8VCNAPBJGEGUKEHW2S4CUNIPKD425WGNGSWVCDKTJV4NJXHA4DUNTSGA5DUDIKKTDFGRJZGI2VGNBUF24DIDIKKTHEGS37G25DIQB3BWFFIVUJIN8VINJTBWFA
cmp_t=00-tips
cmp_tv=902
(App)Cookies=True
(App)Start Menu Shortcuts=False
(App)Safari - Internet History=True
(App)Safari - Cookies=True
(App)Google Chrome - Cookies=True
(App)Google Chrome - Compact Databases=False
SkipUAC=1
HomeScreen=1
DefaultDetailedView=2
BCD=
(Cfg)TTL= 86400
(Cfg)TTL-Spread= 43200
(Cfg)AlphaIntegration=1
(Cfg)ConfigEdgeChromiumCleaning=
(Cfg)GetIpmForTrial=1
(Cfg)HealthCheck=1
(Cfg)HealthCheckIpm=0
(Cfg)HealthCheckShowEvent=0
(Cfg)HealthCheckVersion=1
(Cfg)HideEdgeChromium=0
(Cfg)PC=0
(Cfg)QuickClean=1
(Cfg)QuickCleanIpm=1
(Cfg)SoftwareUpdater=1
(Cfg)SoftwareUpdaterIpm=1
(Cfg)ABTestingNames=|NPTZG7BPQB3GK7TPGA2DC
(Cfg)ccst-prev-001=|IE
(Cfg)LastUpdate=03/22/2020 04:31:00 PM"


# Install new Edge Browser
echo "Do you need to install the new Edge browser (Y/N)?"
Write-Host -Object ($answer = '{0}' -f [System.Console]::ReadKey().Key.ToString());
if ($answer -eq 'Y') {
    start-job -Name "Install New Edge Browser" -scriptblock {
	iwr http://dl.delivery.mp.microsoft.com/filestreamingservice/files/9178ea11-b61e-465a-bc66-158a1868cfe0/MicrosoftEdgeEnterpriseX64.msi -OutFile C:\CCS\Downloads\edge.msi
	Start-Process msiexec.exe -Wait -ArgumentList '/I C:\CCS\Downloads\edge.msi /quiet /L*v* c:\CCS\Downloads\edge_installer.log'
	# launch edge to accept updates after install
	start 'C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe'
 }
}




# Install newest feature update:
Start-Job -Name "Feature Update" -ScriptBlock {
	$dir = 'C:\_Windows_FU\packages'
	New-Item -ItemType Directory -Force -Path $dir
	$webClient = New-Object System.Net.WebClient
	$url = 'https://go.microsoft.com/fwlink/?LinkID=799445'
	$file = "$($dir)\Win10Upgrade.exe"
	$webClient.DownloadFile($url,$file)
	Start-Process -FilePath $file -ArgumentList '/quietinstall /skipeula /auto upgrade /copylogs $dir'
}


# Install PowerShell 7
start-job -Name "Install pwsh7" -ScriptBlock {
if ([System.IO.File]::Exists("c:\Program Files\Powershell\pwsh.exe")){
	echo "PowerShell 7 (pwsh.exe) is already installed."
}
else {
	Invoke-Expression "& { $(Invoke-RestMethod https://aka.ms/install-powershell.ps1) } -Destination 'C:/Program Files/Powershell' -AddToPath"
}
}

# Turn off Microsoft Consumer Experiences
[microsoft.win32.registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\CloudContent", "DisableWindowsConsumerFeatures", 1)
[microsoft.win32.registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\SQMClient\Windows", "CEIPEnable", 0)

# Stop auto-download appx packages
[microsoft.win32.registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\WindowsStore", "AutoDownload", 2)

# Set strong cryptography on 64 bit .Net Framework (version 4 and above)
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Wow6432Node\Microsoft\.NetFramework\v4.0.30319' -Name 'SchUseStrongCrypto' -Value '1' -Type DWord
# Set strong cryptography on 32 bit .Net Framework (version 4 and above)
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\.NetFramework\v4.0.30319' -Name 'SchUseStrongCrypto' -Value '1' -Type DWord


# Download needed packages
Start-Job -Name "Download AdAware"-scriptblock {
	iwr https://files1.majorgeeks.com/10afebdbffcd4742c81a3cb0f6ce4092156b4375/spyware/AdAwareCommandLineScanner.zip -OutFile C:\CCS\Downloads\adaware.zip
	Expand-Archive -Force -LiteralPath C:\CCS\Downloads\adaware.zip -DestinationPath C:\CCS\Downloads\adaware\
	c:/CCS/Downloads/adaware/adawareCommandLineScanner.exe --updatedefs
}

# CCleaner
Start-Job -Name "Download CCleaner" -scriptblock {
	iwr https://download.ccleaner.com/portable/ccsetup565.zip -OutFile C:\CCS\Downloads\ccleaner.zip
	Expand-Archive -Force -LiteralPath C:\CCS\Downloads\ccleaner.zip -DestinationPath C:\CCS\Downloads\ccleaner\
	$ccleanerSettings | Out-File C:\CCS\Downloads\ccleaner\ccleaner.ini
}

# Wise Registry Cleaner
Start-Job -Name "Download Wise Registry Cleaner" -scriptblock {
	iwr http://downloads.wisecleaner.com/soft/WRCFree_10.2.9.689.zip -OutFile C:\CCS\Downloads\wise.zip
	Expand-Archive -Force -LiteralPath C:\CCS\Downloads\wise.zip -DestinationPath C:\CCS\Downloads\wise\
}

# Defender update
Start-Job -Name "Update Defender" -command {
	Update-MpSignature
}

# Remove Appx Packages (A/M/N)
while (1){
	echo "Do you want to remove AppxPackages?  Press A for all (except store), M for most, or N for none."
	Write-Host -Object ($answer = '{0}' -f [System.Console]::ReadKey().Key.ToString());
	if ($answer -eq 'A') {
	    # Remove currently installed Appx
	    Get-AppxPackage -AllUsers | where-object {$_.name -notlike "*Microsoft.WindowsStore*"} | Remove-AppxPackage
	    break
	}
	elseif ($answer -eq 'M') {
		# Remove currently installed Appx (exception of store, calc, scan, mail, photos, InputApp, HP Scan, and HPPrinter)
		Get-AppxPackage -AllUsers | where-object {$_.name -notlike "*Microsoft.WindowsStore*"} | where-object {$_.name -notlike "*Microsoft.WindowsStore*"} | where-object {$_.name -notlike "*Microsoft.WindowsCalculator*"} | where-object {$_.name -notlike "*scan*"} | where-object {$_.name -notlike "*Microsoft.Windows.Photos*"} | where-object {$_.name -notlike "*windowscommunicationsapps*"} | where-object {$_.name -notlike "*pkpass*"} | where-object {$_.name -notlike "*Apple*"} | where-object {$_.name -notlike "*RingDoorbell*"} | where-object {$_.name -notlike "*Printer*"} | where-object {$_.name -notlike "*InputApp*"} | Remove-AppxPackage
	    break
	}
	elseif ($answer -eq 'N') {
	    echo "Moving on...."
	    break
	}
	else {
		echo "Please choose A, M or N (All, Most, or None)"
	}
}


# Remove other things
while (1){
	echo "Do you want to remove OneDrive (Y/N)?"
	Write-Host -Object ($answer = '{0}' -f [System.Console]::ReadKey().Key.ToString());
	if ($answer -eq 'Y') {
	    try {
	        # Uninstall OneDrive
        	taskkill /f /im OneDrive.exe
	        $od = join-path $env:windir SysWOW64\OneDriveSetup.exe
        	start $od /uninstall
	        $od = join-path $env:windir SysWOW32\OneDriveSetup.exe
        	start $od /uninstall
		break

	    }
	    catch {
        	echo "No OneDrive to uninstall"
		break
    	}
}
	elseif ($answer -eq 'N'){
		echo "Skipping OneDrive Removal"
		break
	}
	else {
		echo "Please choose Y or N."
	}
}


try {
	get-package 'Dropbox 20 GB' | Uninstall-package
        get-package 'Dropbox Promotion' | Uninstall-package
}
catch {
       	echo "No DropBox promo to remove."
}

echo "Do you want to remove McAfee Installs (Y/N)?"
Write-Host -Object ($answer = '{0}' -f [System.Console]::ReadKey().Key.ToString());
if ($answer -eq 'Y') {
    try {
        get-package 'McAfee WebAdvisor' | Uninstall-package
}
catch { echo "McAfee Removal 1 failed" }
try {
        get-package 'McAfee LiveSafe' | Uninstall-package
}
catch { echo "McAfee Removal 2 failed" }
try {
        "C:\Program Files\McAfee\Uninstaller.exe"
}
catch { echo "McAfee Removal 3 failed" }

}


echo "Do you want to remove Norton Installs (Y/N)?"
Write-Host -Object ($answer = '{0}' -f [System.Console]::ReadKey().Key.ToString());
if ($answer -eq 'Y') {
    try {
        get-package *norton* | uninstall-package
    }
    catch {
        echo "No Norton to remove."
    }

}


# Wait for all jobs to finish
echo "Waiting for all jobs to complete."
while (1){
	$state = (get-job).State
	if ($state -contains 'Running') {
        clear
	    Get-Job
        sleep 10

}
	else {
        clear
        Get-Job
		break
	}
}




Get-Job | Wait-Job

# Install Windows Update support
c:\Program` Files\Powershell\pwsh.exe -command Install-Module -Name PSWindowsUpdate -Force
c:\Program` Files\Powershell\pwsh.exe -command Install-PackageProvider -name "nuget" -Force

# Run Updates
c:\Program` Files\Powershell\pwsh.exe -command Get-WindowsUpdate -Install -AcceptAll -Verbose -IgnoreReboot

# SFC
echo "Do you want to run SFC (Y/N)?"
Write-Host -Object ($answer = '{0}' -f [System.Console]::ReadKey().Key.ToString());
if ($answer -eq 'Y') {

	c:\Program` Files\Powershell\pwsh.exe -command sfc.exe /scannow
}
else{
	echo "You have skipped running SFC."
}


# AdAware
c:/CCS/Downloads/adaware/adawareCommandLineScanner.exe --boot --disinfect
c:/CCS/Downloads/adaware/adawareCommandLineScanner.exe --quick --quarantine

# CCleaner
c:/CCS/Downloads/ccleaner/CCleaner.exe /AUTO

# Wise Registry Cleaner
c:/CCS/Downloads/wise/Wise` Registry` Cleaner/WiseRegCleaner.exe -a -safe

# DISM
DISM.exe /online /cleanup-image /checkhealth

echo "Do you want to run restorehealth (Y/N)?"
Write-Host -Object ($answer = '{0}' -f [System.Console]::ReadKey().Key.ToString());
if ($answer -eq 'Y') {
    c:\Program` Files\Powershell\pwsh.exe -command DISM.exe /online /cleanup-image /restorehealth
}
else {
    echo "You have skipped DISM restore health."
}

# Defender scan
c:\Program` Files\Powershell\pwsh.exe -command Start-MpScan -ScanType QuickScan

# Notify user to reboot if necessary
$status = c:\Program` Files\Powershell\pwsh.exe -command Get-WURebootStatus -Silent
If ($status) {
    echo "Please restart at your earliest convenience to complete the process."

    }  Else {

    echo "No pending reboot."

  }

echo "Do you want to run chkdisk on next reboot(Y/N)?"
Write-Host -Object ($answer = '{0}' -f [System.Console]::ReadKey().Key.ToString());
if ($answer -eq 'Y') {
    C:\WINDOWS\system32\chkdsk.exe /f /r
}
elseif ($answer -eq 'N') {
	echo "No chkdsk requested"
}

Move-Item c:\CCS\Downloads\MonthlyMaintenance.ps1 c:\CCS\MonthlyMaintenance.ps1 -Force

echo "Reboot now(Y/N)?"
Write-Host -Object ($answer = '{0}' -f [System.Console]::ReadKey().Key.ToString());
if ($answer -eq 'Y') {
    C:\WINDOWS\system32\shutdown.exe /r
}
elseif ($answer -eq 'N') {
	echo "No reboot requested"
}
exit
