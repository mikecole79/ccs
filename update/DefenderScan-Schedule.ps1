﻿# Check status
Get-MpComputerStatus

# Set updates for everyday at 2am
Set-MpPreference -SignatureScheduleDay Everyday
Set-MpPreference -SignatureScheduleTime 120

# Schedule full scan for every Tuesday at 4am
Set-MpPreference -RemediationScheduleDay Tuesday
Set-MpPreference -RemediationScheduleTime 240

# Schedule quick scan every day at 3am
Set-MpPreference -ScanScheduleDay Everyday
Set-MpPreference -ScanScheduleQuickScanTime 180